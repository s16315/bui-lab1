function TableGenerator() {
    var self = this;
    self.htmlTableCode = '';
    self.rows = [];
    self.initTable = function(){
        self.htmlTableCode = '<table>';
    };
    self.addHeader = function (headers) {
        self.htmlTableCode +='<tr>';
        for (var i = 0; i < headers.length; i++) {
            self.htmlTableCode +='<th><button onclick="viewModel.sort(\''+headers[i] + '\')">' + headers[i] + '</button></th>';
        }
        self.htmlTableCode +='</tr>';
    };
    self.addRow = function (row, headers) {
        for (var i = 0; i < headers.length; i++) {
            self.htmlTableCode += '<td>'+ row[headers[i]] +'</td>';
        }
        self.htmlTableCode +='</tr>';
    };
    self.closeTable = function () {
        self.htmlTableCode += '</table>';
    };
    self.makeTable = function(headers, rows){
        self.initTable();
        self.addHeader(headers);
        for (var i = 0; i < rows.length; i++) {
            self.addRow(rows[i], headers);
        }
        self.closeTable();
        return self.htmlTableCode;
    };
}