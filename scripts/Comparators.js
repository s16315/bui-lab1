function Comparators() {
    self = this;
    self.type = '';
    self.increasingOrder = true;
    self.compare = function(object1, object2){
        if (object1.hasOwnProperty(self.type) && object2.hasOwnProperty(self.type)){
            switch (self.type){
                case "id":
                case "income":
                case "age":
                    return Number(object2[self.type])- Number(object1[self.type]);
                case "gender":
                case "firstName":
                case "lastName":
                case "email":
                case "birthsday":
                    return object1[self.type].localeCompare(object2[self.type]);
                default:
                    return object1[self.type].localeCompare(object2[self.type]);            }
        }
        return false;
    };
    self.setType = function (type) {self.type = type;}
}