function PeopleTableViewModel(config) {
    var self = this;
    /**
     * Indeks w @data od którego aktualnie jest wyswietlana tabela
     * @type {number}
     */
    self.currentStartIndex = 0;
    /**
     * Liczba wierszy wyświetlanych jednorazowo w tabeli (strona)
     * @type {*|number}
     */
    self.pageSize = config.pageSize;
    /**
     * Referencja do div-a zawierającego tabelę
     */
    self.context = config.context;
    /**
     * Tablica tymczasowa zawierająca porcję danych do wyświetlenia w tabeli
     * @type {Array}
     */
    self.dataCache = [];
    /**
     * Kolejnośc sortowania
     * @type {boolean}
     */
    self.increasingOrder = false;
    /**
     * Metoda pobierająca stronę danych @self.dataCache
     */
    self.getData = function () {
        self.dataCache = [];
        var end = self.currentStartIndex + self.pageSize;
        if (end > data.length){end = data.length;}
        for (var i = self.currentStartIndex; i < end; i++) {
            self.dataCache.push(data[i]);
        }
    };
    self.sort = function(type){
        var comparator = new Comparators();
        comparator.setType(type);
        self.dataCache.sort(comparator.compare);
        if(self.increasingOrder){
            self.dataCache.reverse();
        }
        self.increasingOrder = !self.increasingOrder;
        self.redraw();
    };
    self.next = function () {
        if(self.currentStartIndex + self.pageSize >=data.length){return;}
        self.currentStartIndex += self.pageSize;
        self.getData();
        self.redraw();
    };
    self.prev = function () {
        if(self.currentStartIndex - self.pageSize <0){ self.currentStartIndex =0;}
        else {self.currentStartIndex -= self.pageSize;}
        self.getData();
        self.redraw();
    };
    self.newRange = function () {
        var list = document.getElementById('rangesList');
        self.pageSize =  Number(list.options[list.selectedIndex].value);
        self.getData();
        self.redraw();
    };
    self.redraw = function () {
        var tableGenerator = new TableGenerator();
        self.context.innerHTML = tableGenerator.makeTable(columnNames, self.dataCache);
    };
    self.init = function () {
        self.getData();
        self.redraw();
    }
}